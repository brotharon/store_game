﻿using UnityEngine;

[CreateAssetMenu(fileName = "ArmorType", menuName = "Item System/Create Properties/Types/Sub Types/Armor Type")]
public class ArmorType : ScriptableObject { }
