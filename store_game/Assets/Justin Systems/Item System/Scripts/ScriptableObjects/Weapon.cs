﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JustinItemSystem;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Item System/Create Item/Weapon")]
public class Weapon : JustinItemSystem.Item {
    public WeaponType weaponType;
}
