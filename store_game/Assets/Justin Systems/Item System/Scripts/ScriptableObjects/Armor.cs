﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JustinItemSystem;

[CreateAssetMenu(fileName = "New Armor", menuName = "Item System/Create Item/Armor")]
public class Armor : JustinItemSystem.Item
{
    public ArmorType armorType;
}
