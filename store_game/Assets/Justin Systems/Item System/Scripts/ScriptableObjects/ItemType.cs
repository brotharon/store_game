﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JustinItemSystem
{
    [CreateAssetMenu(fileName = "ItemType", menuName = "Item System/Create Properties/Types/Item Type")]
    [System.Serializable]
    public class ItemType : ScriptableObject
    {

    }

}