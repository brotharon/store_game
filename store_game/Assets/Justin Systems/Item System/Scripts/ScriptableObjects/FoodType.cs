﻿using UnityEngine;

[CreateAssetMenu(fileName = "FoodType", menuName = "Item System/Create Properties/Types/Sub Types/Food Type")]
public class FoodType : ScriptableObject {}
