﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JustinItemSystem
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Item System/Create Item/Item")]
    [System.Serializable]
    public class Item : ScriptableObject
    {
        new public string name = "New Item";
        public string description = "New Description";
        public Sprite icon;
        public int basePrice = 0;
        public int tier;

        public ItemType type;

        public List<ItemAttributes> attributes;
    }
}
