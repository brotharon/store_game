﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attribute", menuName = "Item System/Create Properties/Item Attribute")]
public class ItemAttributes : ScriptableObject {}
