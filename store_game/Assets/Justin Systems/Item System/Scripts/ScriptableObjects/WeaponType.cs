﻿using UnityEngine;

[CreateAssetMenu(fileName = "WeaponType", menuName = "Item System/Create Properties/Types/Sub Types/Weapon Type")]
public class WeaponType : ScriptableObject {}
