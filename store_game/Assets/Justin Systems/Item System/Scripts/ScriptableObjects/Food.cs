﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JustinItemSystem;

[CreateAssetMenu(fileName = "New Food", menuName = "Item System/Create Item/Food")]
public class Food : JustinItemSystem.Item
{
    public FoodType foodType;
}
