﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JustinItemSystem
{
    [CreateAssetMenu(fileName = "Database", menuName = "Inventory/Create Database")]
    [System.Serializable]
    public class ItemDatabase : ScriptableObject
    {
        public List<Item> itemDB;

    }
}
