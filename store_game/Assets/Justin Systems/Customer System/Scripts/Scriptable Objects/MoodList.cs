﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Occupation", menuName = "Customer System/Customer Properties/Mood List")]
[System.Serializable]
public class MoodList : ScriptableObject {

    public List<Mood> moods = new List<Mood>();
}
