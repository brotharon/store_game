﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Customer", menuName = "Customer System/Create Customer")]
[System.Serializable]
public class Customer : ScriptableObject {

    new public string name;
    [Range(1, 200)]
    public int age;
    public Occupation occupation;
    public MoodList Mood;

}
