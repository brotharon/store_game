﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mood", menuName = "Customer System/Create Mood")]
[System.Serializable]
public class Mood : ScriptableObject
{
    [Range(0f,10f)]
    public float value;
}