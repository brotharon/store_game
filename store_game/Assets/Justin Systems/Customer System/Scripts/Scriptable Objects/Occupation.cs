﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Occupation", menuName = "Customer System/Create Occupation")]
[System.Serializable]
public class Occupation : ScriptableObject { }