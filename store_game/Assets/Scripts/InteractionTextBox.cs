﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionTextBox : MonoBehaviour {

	public GameObject dbox;
	public Text dText;

	public bool dialogueActive;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//turn off the press E text box if E is triggered
		if (dialogueActive && Input.GetKeyDown (KeyCode.E)) {

			dbox.SetActive (false);
			dialogueActive = false;
		}

	}

	/// <summary>
	/// Presses the e. appear text box
	/// </summary>
	public void PressE(){
		dialogueActive = true;
		dbox.SetActive(true);

	}
	/// <summary>
	/// Presses the E off. dissappear the text box
	/// </summary>
	public void PressEOff(){
		dialogueActive = false;
		dbox.SetActive(false);

	}
	/*
	public void ShowBox(string dialogue){
		dialogueActive = true;
		dbox.SetActive (true);
		dText.text = dialogue;
	}*/
}
