﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class BarterUIBehavior : MonoBehaviour 
{
	public Flowchart flowchart;
	public bool barteringOccurring;
	public GameObject upButton;
	public GameObject downButton;
	public GameObject tenThousandsPlace;
	public GameObject hundredThousandsPlace;
	public GameObject millionsPlace;
	public GameObject thousandsPlace;
	public GameObject hundredsPlace;
	public GameObject tensPlace;
	public GameObject onesPlace;
	public int currentPlace = 0;
	public GameObject[] places; 

	// Use this for initialization
	void Start () 
	{
		currentPlace = 3;
		places = new GameObject[] {millionsPlace, hundredThousandsPlace, tenThousandsPlace, 
			thousandsPlace, hundredsPlace, tensPlace, onesPlace};
	}
	
	// Update is called once per frame
	void Update () 
	{
		barteringOccurring = flowchart.GetBooleanVariable("barteringOccurring");
		Debug.Log (currentPlace);
		//float horizontalAxis = Input.GetAxis("Horizontal"); //A and D
		//float verticalAxis = Input.GetAxis("Vertical");//W and S 

		Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		if (barteringOccurring) 
		{
			
		}

		if (stickInput.y > 0.8) 
		{
			//Debug.Log ("Down");
		}
		else if (stickInput.y < -0.8) 
		{
			//Debug.Log ("Up");
		}
		if (stickInput.x > 0.8) {
			Debug.Log ("Left");
			MoveArrowsLeft ();
		}
		else if (stickInput.x < -0.8) 
		{
			Debug.Log ("Right");
			MoveArrowsRight();
		}
	}
	void MoveArrowsLeft()
	{
		if (currentPlace > 0 && currentPlace <= 7) 
		{
			currentPlace--; 
		}
	}

	void MoveArrowsRight()
	{
		if (currentPlace >= 0 && currentPlace < 7) 
		{
			currentPlace++;
		}
	}
}
