﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {


	public Transform target;
	public float smoothedSpeed;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
		smoothedSpeed = .25f;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 desiredPosition = target.position + offset;
		Vector3 smoothedPosition = Vector3.Lerp (transform.position, desiredPosition, smoothedSpeed);
		transform.position = smoothedPosition;
	}
}
