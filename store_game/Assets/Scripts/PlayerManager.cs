﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    Animator anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.W))
        {
            anim.SetInteger("State", 1);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            anim.SetInteger("State", 2);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            anim.SetInteger("State", 3);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            anim.SetInteger("State", 4);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            anim.SetInteger("State", 1);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            anim.SetInteger("State", 2);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            anim.SetInteger("State", 4);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            anim.SetInteger("State", 3);
        }
        if (Input.anyKey == false)
        {
            anim.SetInteger("State", 0);
        }
    }
}
