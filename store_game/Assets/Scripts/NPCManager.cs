﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NPCManager : MonoBehaviour {

    Animator anim;
    Vector3 position;
    float x;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        position = gameObject.transform.parent.position;
        x = position.x;

    }
	
	// Update is called once per frame
	void Update ()
    {
		
		Vector3 updatePosition = gameObject.transform.parent.position;
		float x2 = updatePosition.x;

		if (x2 > x)
		{
			x = x2;
			anim.SetInteger("State", 3);
		}
		else if (x2 < x)
		{
			x = x2;
			anim.SetInteger("State", 4);
		}
		else if (x2 == x)
		{
			anim.SetInteger("State", 0);
		}
    }
}
