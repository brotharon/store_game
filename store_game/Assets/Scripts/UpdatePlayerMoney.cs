//This script can be placed on text components to update between the variable in the flowchart and UI

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;
using System;

public class UpdatePlayerMoney : MonoBehaviour 
{
	public Flowchart flowchart;
	public int playerMoney;

	// Use this for initialization
	void Start () 
	{
		flowchart.SetIntegerVariable("playerMoney", 0); 
		gameObject.GetComponent<Text>().text = "$XXXXXXX";
	}

	void LateUpdate () 
	{
		playerMoney = flowchart.GetIntegerVariable("playerMoney");
		gameObject.GetComponent<Text> ().text = Convert.ToString(playerMoney);
		//Debug.Log("Player's Current Money: " + playerMoney);
	}
}
