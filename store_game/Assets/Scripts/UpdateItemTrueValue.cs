//This script can be placed on text components to update between the variable in the flowchart and UI

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;
using System;

public class UpdateItemTrueValue : MonoBehaviour 
{
	public Flowchart flowchart;
	public int itemValue;

	// Use this for initialization
	void Start () 
	{
		//flowchart.SetIntegerVariable("trueItemValue", 0); 
		//gameObject.GetComponent<Text>().text = "$XXXXXXX";
	}

	void SetTrueValue () 
	{
		itemValue = flowchart.GetIntegerVariable("trueItemValue");
		gameObject.GetComponent<Text>().text = Convert.ToString(itemValue);
		//Debug.Log("Player's Current Money: " + playerMoney);
	}
}
