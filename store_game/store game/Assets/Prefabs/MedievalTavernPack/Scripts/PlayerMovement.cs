﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {



	public float playerSpeed;
	private Rigidbody rig;

	// Use this for initialization
	void Start () {
		rig = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		float horizontalAxis = Input.GetAxis ("Horizontal");
		float verticalAxis = Input.GetAxis ("Vertical") * (-1);//flip the W and S 

		//								this is x	  y     z (technicly y axis)
		Vector3 movement = new Vector3 (verticalAxis, 0, horizontalAxis) * playerSpeed * Time.deltaTime;
		rig.MovePosition (transform.position + movement);
		//transform.Translate (playerSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, playerSpeed * Input.GetAxis("Vertical")  * Time.deltaTime);
	}
}
